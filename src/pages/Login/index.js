import { useState } from 'react';
import { Button, Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import Login_logo from '../../assets/images/new.png';
import "../Login/login.scss";

const Login = () => {
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const onFinish = (values) => {
        setLoading(true);

        if (values.email === "admin@ssvtraders.com" && values.password === "Sherwin748494") {
            navigate('/home');
            message.success("Successful Login")
        } else {
            message.error("Invalid email or password");
        }
        
        setLoading(false);
    };

    return (
        <>
            <div className="login">
                <div className="login-welcomeSession">
                    <div className="login-welcomeSession-box">
                        <div className='login-welcomeSession-title'>
                            {/* <h2>Welcome to</h2> */}
                        </div>
                        <div className='login-welcomeSession-logoBox'>
                            <img src={Login_logo} width={300} alt=" " />
                        </div>
                        <div className='login-welcomeSession-quoteBox'>
                            <p>
                                Lord causeth the grass to grow for the cattle,
                                and herb for the service of man:
                                that he may bring forth food out of the earth;
                                And oil to make his face to shine, and bread which strengtheneth
                                man's heart!</p>
                                <p>Psalm 104:14</p>
                        </div>
                    </div>
                </div>
                <div className="login-box">
                    <div className="login-loginSession">
                        <div style={{ display: 'flex' }}>
                            <div className="login-form">
                                <h1>Login</h1>
                                <Form
                                     onFinish={onFinish}
                                    layout="vertical" autoComplete="off">
                                    <div className='login-inputBox'>
                                        <Form.Item name="email" rules={[{ type: "email", required: true, message: "Please enter your Email ID", }]}>
                                            <Input size="large" prefix={<UserOutlined className='inputLogo' />} placeholder="Email ID" className="login-input" />
                                        </Form.Item>
                                        <Form.Item name="password" rules={[{ required: true, message: "Please enter your password!" }]}>
                                            <Input.Password placeholder="Password" prefix={<LockOutlined className='inputLogo' />} className="login-input" />
                                        </Form.Item>
                                    </div>
                                    <div className='login-action'>
                                     
                                        <Button htmlType="submit" loading={loading} className="login-button" >LogIn</Button>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Login;
