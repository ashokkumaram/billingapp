import React, { useState } from 'react';
import { Form, Input, DatePicker, Button, Row, Col, Table, Switch, InputNumber, Modal, Space, Select } from 'antd';
import moment from 'moment';
import { DeleteOutlined } from '@ant-design/icons';
import "../Billing/bill.scss";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';
import LOGO from "../../assets/images/new.jpg"
import PAGE from "../../assets/images/logo.jpeg"
import CROSS from "../../assets/images/cross.jpg"
import PopImage from "../../assets/images/tick-circle.jpg"

const Home = () => {

  const [data, setData] = useState([]);
  const [sgst, setSgst] = useState(0);
  const [cgst, setCgst] = useState(0);
  const [igst, setIgst] = useState(0);
  const [isOtherState, setIsOtherState] = useState(false);

  const handleToggleChange = (checked) => {
    setIsOtherState(checked);
  };

  const handleAdd = () => {
    setData([
      ...data,
      {
        key: data.length + 1,
        productName: '',
        hsnCode: '',
        price: 0,
        kg: '',
        gsts: '0',
        quantity: 0,
        amount: 0,
      },
    ]);
  };

  const handleDelete = (record) => {
    setData(data.filter((item) => item.key !== record.key));
  };

  const handleProductNameChange = (e, record) => {
    const { value } = e.target;
    const newData = data.map((item) =>
      item.key === record.key ? { ...item, productName: value } : item
    );
    setData(newData);
  };

  const handleHsnCodeChange = (value, record) => {
    const newData = data.map((item) =>
      item.key === record.key ? { ...item, hsnCode: value } : item
    );
    setData(newData);
  };

  const handleKgchange = (e, record) => {
    const { value } = e.target;
    const newData = data.map((item) =>
      item.key === record.key ? { ...item, kg: value } : item
    );
    setData(newData);
  };
  const handleGstChange = (value, record) => {
    const newData = data.map((item) => {
      if (item.key === record.key) {
        const sgst = value / 2;
        const cgst = value / 2;
        const igst = value;
        console.log(igst);
        const newAmount = item.price * item.quantity;
        const gstAmount = (newAmount * value) / 100;
        // const totalAmount = newAmount;
        setIgst(igst);
        setSgst(sgst);
        setCgst(cgst);
        return { ...item, gsts: value, sgst, cgst, igst, gstAmount };
      }
      return item;
    });
    setData(newData);
  };

  const handlePriceChange = (value, record) => {
    const newData = data.map((item) => {
      if (item.key === record.key) {
        const newAmount = value * item.quantity;
        const gstAmount = (newAmount * item.gsts) / 100;
        const totalAmount = newAmount ;
        const taxableAmount = newAmount;
        return { ...item, price: value, amount: totalAmount, gstAmount, taxableAmount };
      }
      return item;
    });
    setData(newData);
  };

  const handleQuantityChange = (value, record) => {
    const newData = data.map((item) => {
      if (item.key === record.key) {
        const newAmount = item.price * value;
        const gstAmount = (newAmount * item.gsts) / 100;
        const totalAmount = newAmount ;
        return { ...item, quantity: value, gstAmount, amount: totalAmount };
      }
      return item;
    });
    setData(newData);
  };

  const totalAmount = data.reduce((acc, curr) => {
    const amountWithoutGST = curr.amount ;
    return acc + amountWithoutGST;
  }, 0).toFixed(0);

  const grandGst = data.reduce((acc, curr) => {
    const sgst = curr.gstAmount;
    return acc + sgst / 2
  }, 0).toFixed(2)

  const grandIGst = data.reduce((acc, curr) => {
    const sgst = curr.gstAmount;
    return acc + sgst
  }, 0).toFixed(2)

  const grandAmount = isOtherState ? grandIGst : 0;
  const grandSGST = isOtherState ? 0 : grandGst;

  const grandAmountGST = isOtherState ? igst : 0;
  const grandSGST2 = isOtherState ? 0 : sgst;

  const grandTotal = (Number(totalAmount) + Number(grandGst * 2)).toFixed(0);

  function convertToIndianNumberingSystem(number) {
    const units = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    const teens = ['', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    const tens = ['', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    function convertGroupOfThree(num) {
      let result = '';
      let hundred = Math.floor(num / 100);
      let remaining = num % 100;

      if (hundred > 0) {
        result += units[hundred] + ' Hundred ';
      }

      if (remaining > 10 && remaining < 20) {
        result += teens[remaining - 10] + ' ';
      } else {
        let ten = Math.floor(remaining / 10);
        let unit = remaining % 10;
        if (ten > 0) {
          result += tens[ten] + ' ';
        }
        if (unit > 0) {
          result += units[unit] + ' ';
        }
      }

      return result.trim();
    }

    function convertNumberToWords(num) {
      if (num === 0) {
        return 'Zero';
      }

      let crore = Math.floor(num / 10000000);
      let lakh = Math.floor((num % 10000000) / 100000);
      let thousand = Math.floor((num % 100000) / 1000);
      let remainder = num % 1000;
      let result = '';

      if (crore > 0) {
        result += convertGroupOfThree(crore) + ' Crore ';
      }
      if (lakh > 0) {
        result += convertGroupOfThree(lakh) + ' Lakh ';
      }
      if (thousand > 0) {
        result += convertGroupOfThree(thousand) + ' Thousand ';
      }
      if (remainder > 0) {
        result += convertGroupOfThree(remainder);
      }

      return result.trim();
    }

    return convertNumberToWords(number);
  }

  const rupees = convertToIndianNumberingSystem(grandTotal)
  const words = rupees.split(' ');
  const breakIndex = 4;
  const formattedRupees = words.slice(0, breakIndex).join(' ') + '\n' + words.slice(breakIndex).join(' ');

  const columns = [
    {
      title: 'S.No',
      dataIndex: 'sno',
      render: (_, __, index) => index + 1,
      className: 'hide-on-small-screen' 
    },
    {
      title: 'Product',
      dataIndex: 'productName',
      render: (_, record) => <Input value={record.productName} onChange={(e) => handleProductNameChange(e, record)} />,
    },
    {
      title: 'Kilogram',
      dataIndex: 'kg',
      render: (_, record) => <Input value={record.kg} onChange={(e) => handleKgchange(e, record)} />,
    },
    {
      title: 'HSN',
      dataIndex: 'hsncode',
      render: (_, record) => (
        <InputNumber min={0} onChange={(value) => handleHsnCodeChange(value, record)} />
      ),
    },
    {
      title: 'GST (%)',
      dataIndex: 'gsts',
      render: (_, record) => (
        <InputNumber min={0} className="gst-input" onChange={(value) => handleGstChange(value, record)} />
      ),
    },
    {
      title: 'Price',
      dataIndex: 'price',
      render: (_, record) => (
        <InputNumber min={0} onChange={(value) => handlePriceChange(value, record)} />
      ),
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      render: (_, record) => (
        <InputNumber min={0} onChange={(value) => handleQuantityChange(value, record)} />
      ),
    },

    {
      title: 'Amount',
      dataIndex: 'amount',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: (_, record) => (
        <Button onClick={() => handleDelete(record)} icon={<DeleteOutlined />} danger />
      ),
    },
  ];


  const onFinish = (values) => {

    let dueDate = new Date(values?.dueDate)
    let date = moment(dueDate).format("DD-MM-YYYY")
    values.dueDate = date
    let billDate = new Date(values?.date)
    let bdate = moment(billDate).format("DD-MM-YYYY")
    values.date = bdate
    values.tableData = data.map(item => ({
      key: item.key,
      productName: item.productName,
      hsnCode: item.hsnCode,
      price: item.price,
      kg: item.kg,
      gst: item.gsts,
      quantity: item.quantity,
      amount: item.amount
    }));
    values.totalAmount = totalAmount;
    values.grandTotal = grandTotal;
    setTimeout(() => {
      const content = document.getElementById('pdf-content');
      if (content) {
        generatePDF(values, data, totalAmount, grandTotal);

      } else {
        console.error('Element with id "pdf-content" not found.');
      }
    }, 1000);
    console.log('Received values:', values);

  };


  const generatePDF = (values, data, totalAmount, grandTotal) => {
    const pdf = new jsPDF();
    const content = document.getElementById('pdf-content');
    if (!content) {
      console.error('Invalid element:', content);
      return;
    }

    html2canvas(content).then(canvas => {

      pdf.setFontSize(10);
      pdf.setFont("helvetica", "bold");
      pdf.text(`GSTIN`, 20, 10);
      pdf.text(`: 33DVXPS0787P1ZA`, 43, 10);
      pdf.setFontSize(10);
      pdf.setFont("helvetica", "normal");
      pdf.text(`State Code`, 20, 15);
      pdf.text(`: 33`, 43, 15);
      const CrossImg = CROSS

      pdf.setFontSize(10);
      const lineHeight = 15;
      const maxWidth = pdf.internal.pageSize.getWidth();
      const rightMargin = 20;
      const maxAllowedX = maxWidth - rightMargin;
      const companyLogoData = LOGO
      pdf.addImage(companyLogoData, 'JPEG', 18, 18, 33, 33);

      pdf.setFontSize(12);
      pdf.setFont("helvetica", "bold");
      const invoiceTextTWidth = pdf.getTextWidth("Cash / Credit Bill");
      const invoiceTTextX = maxAllowedX - invoiceTextTWidth;
      pdf.text(`${values.cash} Bill`, invoiceTTextX + 15, 20);
      pdf.addImage(CrossImg, 'JPEG', invoiceTTextX - 55, 5);
      pdf.setFontSize(10);
      pdf.setFont("helvetica", "normal");
      pdf.text("Phone No  :", invoiceTTextX - 10, 10);
      pdf.text("99761 02562", invoiceTTextX + 15, 10);
      pdf.text("86107 82569", invoiceTTextX + 15, 15);
      pdf.text(`Type of Bill:`, invoiceTTextX - 10, 20);

      pdf.setFontSize(24);
      pdf.setFont("helvetica", "bold");
      const invoiceTextWidth = pdf.getTextWidth("S.S.V. Traders");
      const invoiceTextX = 27 + invoiceTextWidth;
      pdf.text("S.S.V. Traders", invoiceTextX, 30);

      pdf.setFontSize(10);
      pdf.setFont("helvetica", "normal");
      pdf.text(`59/34-C, Krishnamachary Road, Fathima Nagar, VIRUDHUNAGAR-626001.`, 56, 36);

      pdf.setFontSize(10);
      pdf.setFont("helvetica", "normal");
      pdf.text(`Godown: 3/501-1, Kulloorchanthai Road, VIRUDHUNAGAR-626 001.`, 62, 42);

      const lineX = 125;
      const lineY = 37 + lineHeight;
      pdf.line(20, lineY, maxAllowedX, lineY);
      pdf.setFontSize(12);
      pdf.setFont("helvetica", "bold");
      pdf.text(`Bill To:-`, 20, 60);
      pdf.setFontSize(10);
      pdf.setFont("helvetica", "normal");
      pdf.text(`${values.clientaddress}`, 20, 64);
      pdf.text("GST No", 20, 86);
      pdf.text(`State Code`, 20, 91);
      pdf.text(`Phone No`, 20, 81);
      pdf.text(`: ${values.clientgstin ? values.clientgstin : ''}`, 45, 86);
      pdf.text(`: ${values.code ? values.code : ''}`, 45, 91);
      pdf.text(`: ${values.clientphone ? values.clientphone : ''}`, 45, 81);

      const invoiceId = `INVOICE No`;
      const invoiceIdNum = `: ${values.invoiceNo}`;
      pdf.text(invoiceId, 120, 60);
      pdf.text(invoiceIdNum, 150, 60);

      const duedateEText = `E-Way Bill No`;
      const duedateENumber = values.vechile ? `: ${values.ewaybill}` : ': -';
      pdf.text(duedateEText, 120, 66);
      pdf.text(duedateENumber, 150, 66);

      const dateText = `Date`;
      const dateDate = `: ${values.date}`;
      pdf.text(dateText, 120, 72);
      pdf.text(dateDate, 150, 72);

      const duedateText = `Vehicle No`;
      const duedateNumber = values.vechile ? `: ${values.vechile.toUpperCase()}` : ': -';
      pdf.text(duedateText, 120, 78);
      pdf.text(duedateNumber, 150, 78);      
      const lineT = 80 + lineHeight;
      pdf.line(lineX - 20, lineY, lineX - 20, lineT);
      pdf.line(20, lineT, maxAllowedX, lineT);
      pdf.setFillColor(200, 200, 200);
      pdf.rect(20, lineT, maxAllowedX - 20, 10, 'F');
      const tableHeaders = ["S.No", "Particulars", "KG", "HSN", "GST", "Price", "Quantity", "Amount"];

      const lineD = 90 + lineHeight;
      pdf.line(20, lineD, maxAllowedX, lineD);

      const tableData = [];
      data.forEach(item => {
        tableData.push([item.key, item.productName, item.kg, item.hsnCode,  `${item.gsts} %`, item.price, item.quantity, item.amount]);
      });

      const tableStartX = 20;
      const tableStartY = 102;
      const columnWidths = [12, 35, 15, 30, 15, 20, 20, 25];
      let currentX = tableStartX;
      tableHeaders.forEach((header, index) => {
        pdf.text(header, currentX, tableStartY);
        currentX += columnWidths[index];
      });
      const totalTableWidth = columnWidths.reduce((sum, width) => sum + width, 0);

      // This variable is used to calculate the subtotal for PDF printing
      // eslint-disable-next-line no-unused-vars
      let subtotal = 0;

      tableData.forEach((rowData, rowIndex) => {
        currentX = tableStartX;
        const lineHeight = 10;
        const tableRowY = tableStartY + (rowIndex + 1) * lineHeight;

        pdf.text(String(rowIndex + 1), tableStartX, tableRowY);

        rowData.forEach((cell, cellIndex) => {
          pdf.text(String(cell), currentX, tableRowY);
          currentX += columnWidths[cellIndex];
          if (cellIndex === 3) {
            subtotal += parseFloat(cell);
          }
        });
      });

      const linez = tableStartY + (tableData.length) * lineHeight;
      pdf.line(20, linez, tableStartX + totalTableWidth, linez);
      const paymentInstructionY = linez + 10;
      pdf.setFont("helvetica", "bold");
      pdf.text("Our Bank Details", 20, paymentInstructionY);
      const firstTextHeight = 5;
      const verticalSeparation = 5;

      const nextRowY = paymentInstructionY + firstTextHeight + verticalSeparation;
      pdf.setFont("helvetica", "normal");
      pdf.text(`Bank of India, Virudhunagar Branch`, 20, nextRowY);

      const nextRow1Y = nextRowY + firstTextHeight + verticalSeparation;
      pdf.text(`A/c. No. 826620110000096`, 20, nextRow1Y);

      const nextRow2Y = nextRow1Y + firstTextHeight + verticalSeparation;
      pdf.text(`IFSC Code: BKID0008266`, 20, nextRow2Y);

      const nextRow3Y = nextRow2Y + firstTextHeight + verticalSeparation;
      pdf.text(`E & O.E.`, 20, nextRow3Y);

      const lineBelowTextBlockY = nextRow3Y + verticalSeparation;

      pdf.line(20, lineBelowTextBlockY, tableStartX + totalTableWidth, lineBelowTextBlockY);

      pdf.line(lineX - 20, linez, lineX - 20, lineBelowTextBlockY);

      pdf.setFont("helvetica", "bold");
      const nextRow4Y = nextRow3Y + firstTextHeight + verticalSeparation
      pdf.text(`Rupees ${formattedRupees} Only`, 20, nextRow4Y);
      pdf.setFont("helvetica", "normal");
      pdf.text(`Amount`, invoiceTTextX - 35, paymentInstructionY, { align: 'left' });
      pdf.text(`: ${totalAmount} / -`, invoiceTTextX, paymentInstructionY, { align: 'left' });
      pdf.text(`SGST ${grandSGST2} %`, invoiceTTextX - 35, nextRowY, { align: 'left' });
      pdf.text(`: ${grandSGST} / -`, invoiceTTextX, nextRowY, { align: 'left' });
      pdf.text(`IGST ${grandAmountGST} %`, invoiceTTextX - 35, nextRowY + 20, { align: 'left' });
      pdf.text(`: ${grandAmount} / -`, invoiceTTextX, nextRowY + 20, { align: 'left' });
      pdf.text(`CGST ${grandSGST2} %`, invoiceTTextX - 35, nextRowY + 10, { align: 'left' });
      pdf.text(`: ${grandSGST} / -`, invoiceTTextX, nextRowY + 10, { align: 'left' });
      pdf.setFont("helvetica", "bold");
      pdf.text(`Total Amount`, invoiceTTextX - 35, nextRowY + 30, { align: 'left' });
      pdf.text(`: ${grandTotal} / -`, invoiceTTextX, nextRowY + 30, { align: 'left' });
      pdf.text(`S.S.V. Traders`, 185, nextRowY + 70, { align: 'right' });
      pdf.setFont("helvetica", "normal");
      pdf.text(`For`, 154, nextRowY + 70);

      const lineAfterScan = nextRowY + 70 + firstTextHeight + verticalSeparation;
      pdf.line(20, lineAfterScan, maxAllowedX, lineAfterScan);
      const nextRow6Y = lineAfterScan + verticalSeparation;
      pdf.setFont("helvetica", "normal");
      pdf.text(`This is a Computer Generated Bill`, 75, nextRow6Y);
      // pdf.save('invoice.pdf');
      pdf.autoPrint();
      const blob = pdf.output('blob');
      const url = URL.createObjectURL(blob);
      const iframe = document.createElement('iframe');
      iframe.style.display = 'none';
      iframe.src = url;
      document.body.appendChild(iframe);
      iframe.contentWindow.print();
    });
    setIsModalVisible(false);


  };

  const handleClear = () => {
    window.location.reload();
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleGenerateInvoice = () => {

  };

  const handleModalOk = () => {
    setIsModalVisible(false);
  };

  const handleModalCancel = () => {
    setIsModalVisible(false);
  };


  return (
    <>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img src={PAGE} alt="Company Logos" style={{ width: '100px', height: 'auto' }} />
        <h3 style={{ marginLeft: '10px' }}>Invoice Generator</h3>
      </div>
      <div id="pdf-content" >
        <Form
          name="invoice_form"
          onFinish={onFinish}
        >
          <Row gutter={16}>
            <Col span={7}>
              <Form.Item
                label="Invoice No:"
                name="invoiceNo"
                required={false}
                rules={[{ required: true, message: "Invoice No Required" }]}
                className="employee-details_form_dub"
                colon={false}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="E-Way Bill No"
                name="ewaybill"
                required={false}
                rules={[{ required: false, message: "Eway Bill No Required" }]}
                className="employee-details_form_dub"
                colon={false}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Bill Date:"
                name="date"
                required={false}
                rules={[{ required: true, message: "Date Required" }]}
                className="employee-details_form_dub"
              >
                <DatePicker format="DD-MM-YYYY" />
              </Form.Item>
              <Form.Item
                label="Credit/Cash"
                name="cash"
                required={false}
                rules={[{ required: true, message: "Select Option" }]}
                className="employee-details_form_dub"
              >
                <Select>
                  <Select.Option value="Credit">Credit</Select.Option>
                  <Select.Option value="Cash">Cash</Select.Option>
                </Select>
              </Form.Item>
              <div className="your-container" style={{marginLeft: '40px'}}>
                <span>IGST:</span>
                <Switch
                  checked={isOtherState}
                  onChange={handleToggleChange}
                  checkedChildren="Other State"
                  unCheckedChildren="Within State"
                />
              </div>
            </Col>
            <Col span={7}>
              <Form.Item
                label="Client Details"
                name="clientaddress"
                required={false}
                rules={[{ required: true, message: "Address Required" }]}
                className="employee-details_form_dub"
              >
                <Input.TextArea rows={5} />
              </Form.Item>

            </Col>
            <Col span={7}>
              <Form.Item
                label="Phone"
                name="clientphone"
                required={false}
                rules={[{ required: false, message: "Phone No Required" }]}
                className="employee-details_form_dub"
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Client GST No"
                name="clientgstin"
                required={false}
                rules={[{ required: false, message: "GST No Required" }]}
                className="employee-details_form_dub"
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="State Code"
                name="code"
                required={false}
                rules={[{ required: false, message: "State Code Required" }]}
                className="employee-details_form_dub"
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Vechile No"
                name="vechile"
                required={false}
                rules={[{ required: false, message: "Vechile No Required" }]}
                className="employee-details_form_dub"
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Table dataSource={data} columns={columns} pagination={false}/>
          <div className="employee-details_submit">
            <Button className="employee-details_submit-btn" onClick={handleAdd} >
              + Add row
            </Button>
          </div>
          <div style={{ marginTop: 16 }} className="employee-details_submit">
            Total Amount: {totalAmount}
          </div>

          <div style={{ marginTop: 16 }} className="employee-details_submit">
            {isOtherState ? (
              <>
                IGST % : {igst}%
                IGST Amount: {grandAmount}
              </>
            ) : (
              <>
                SGST % : {sgst}%
                SGST Amount: {grandSGST}
                <br />
                CGST % : {cgst}%
                CGST Amount: {grandSGST}
              </>
            )}
          </div>
          <div style={{ marginTop: 16 }} className="employee-details_submit">
            Grand Total: {grandTotal}
          </div>
          <div style={{ marginTop: 16 }} className="employee-details_submit">
            Rupees: {formattedRupees} only
          </div>
          <Form.Item>
            <div className="employee-details_submit">
              <Button onClick={handleClear} className="employee-details_cancel-btn">
                Clear
              </Button>
              <Button htmlType="submit" className="employee-details_submit-btn" onClick={handleGenerateInvoice}>
                Generate Invoice
              </Button>

            </div>
          </Form.Item>

        </Form>

      </div>

      <Modal
        open={isModalVisible}
        title=""
        onOk={handleModalOk}
        onCancel={handleModalCancel}
        footer={[
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Button
              key="submit"
              type="primary"
              // loading={loading}
              onClick={handleModalOk}
              style={{
                display: "flex",
                width: "206px",
                padding: "15px 30px",
                justifyContent: "center",
                alignItems: "center",
                gap: "10px",
                borderRadius: "8px",
                background: "#128c7e",
              }}
            >
              OK
            </Button>
          </div>,
        ]}
        width={"386px"}
      >
        <Space
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <img
            src={PopImage}
            alt=""
            style={{
              width: "150px",
              height: "150px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          />
          <p
            style={{
              color: "#101010",
              textAlign: "center",
              fontFamily: "revert",
              fontSize: "28px",
              fontStyle: "normal",
              fontWeight: "700",
              lineHeight: "normal",
            }}
          >
            Invoice has been generated successfully!
          </p>
        </Space>
      </Modal>
    </>
  );
};

export default Home;
